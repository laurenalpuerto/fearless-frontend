window.addEventListener('DOMContentLoaded', async () => {
  class LocationForm extends React.Component {
    constructor(props) {
      super(props)
      this.state = { states: [] };
      this.handleNameChange = this.handleNameChange.bind(this);
    }

    handleNameChange(event) {
      const value = event.target.value;
      this.setState({ name: value })
    }

    componentDidMount() {
      const url = 'http://localhost:8000/api/states/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        this.setState({ states: data.states });
      }
    }

    // render method...

  }
})