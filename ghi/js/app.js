window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();
      console.log(data.conferences)
      const conference = data.conferences[0];
      const nameTag = document.querySelector('.card-title');
      nameTag.innerHTML = conference.name;
      console.log(conference.href)
      const detailUrl = `http://localhost:8000${conference.href}`;
      const detailResponse = await fetch(detailUrl);
      if (detailResponse.ok) {
        const details = await detailResponse.json();
        // W9D2D2: get the description data out of the object
        const conferenceDetail = details.conference;
        // use querySelector to select HTML element that should hold the description
        const detailTag = document.querySelector('.card-textp');
        // set the innerHTML property of the HTML element to the description of the conference
        const locationResponse = await fetch(`http://localhost:8000${details.conference.location.href}`);
        if (locationResponse.ok) {
          const location = await locationResponse.json();
          detailTag.innerHTML = conferenceDetail.description;
          const imageTag = document.querySelector('.card-img-top');
          imageTag.src = location.picture_url;
        }
      }

    }
  } catch (e) {
    // Figure out what to do if an error is raised
    //console.log(e);
  }

});